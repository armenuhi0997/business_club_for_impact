import { Component, Inject } from '@angular/core';
import { NavController, NavParams, ModalController, LoadingController } from 'ionic-angular';
import { ConversationModal, CreateConversationPage } from '../../modals/barrel';
import { RequestService } from '../../services/request.service';
import { Conversation, User } from '../../types/types';
import { map } from 'rxjs/operators';
import { Subscription } from 'rxjs';
import { forkJoin } from 'rxjs/observable/forkJoin';

@Component({
  selector: 'page-conversation-list',
  templateUrl: 'conversation-list.html',
})
export class ConversationListPage {
  public clubId: number;
  public clubName: string;
  private _subscription: Subscription;
  public height:string;
  public sum:number;
  public conversations: Array<Conversation> = []
  constructor(public navCtrl: NavController,
    private _loadingCtrl: LoadingController,
    private _requestService: RequestService,
    public navParams: NavParams,
    private _modalCtrl: ModalController,
    @Inject('FILE_URL') public fileUrl
  ) {
    this.clubId = this.navParams.get("clubId");
    this.clubName = this.navParams.get("clubName")
  }
  ngOnInit() {
    this.navCtrl.getPrevious().data.isRead = false
    this._combineObservable()
  }
  private _combineObservable() {
    const combine = forkJoin(
      this._getConversation(false, 'name')
    )
    this._subscription = combine.subscribe()
  }
  /**
   * 
   * @param isSearch 
   * @param name 
   */
  private _getConversation(isSearch: boolean, name: string) {
    let loadCtrl = this._loadingCtrl.create();
    loadCtrl.present()
    return this._requestService.getConversationsByClubId(isSearch, name, this.clubId).pipe(
      map((data: { messages: Conversation[], status: string, sum: number }) => {
        loadCtrl.dismiss()
        console.log(data);
        this.navCtrl.getPrevious().data.isRead = true
        this.conversations = data["messages"];
        this.sum=data.sum
        return data
      })
    )
  }

  /**
   * 
   * @param event 
   */
  public filter(event: string) {
    if (event) {
      this._getConversation(true, event).subscribe()
    }
  }
  empty(ev) {
    this._getConversation(false, 'name').subscribe()
  }
  public getHeight(event){
    this.height=event+'px';
    console.log(this.height);    
  }
  public openConvaresation(name: string, id: number, count: number, isCreate: boolean) {
    console.log(count);

    let modal = this._modalCtrl.create(ConversationModal, { name: name, clubName: this.clubName, conversationId: id, clubId: this.clubId, count: count });
    modal.onWillDismiss((result) => {
      if (result) {
        console.log(result);
        // this.navCtrl.getPrevious().data.isRead = true
        this._getConversation(false, 'name').subscribe();
      } else {
        if (isCreate) {
          this._getConversation(false, 'name').subscribe();
        }
      }
    })
    modal.present()
  }
  public createConversation() {
    let modal = this._modalCtrl.create(CreateConversationPage, { clubId: this.clubId, clubName: this.clubName });
    modal.onWillDismiss((result: { name: string, id: number }) => {
      if (result) {
        this.openConvaresation(result.name, result.id, 0, true)
      }
    })
    modal.present()
  }
  public readAllMessage() {
    return this._requestService.deleteAllUnreadMessageCount(this.clubId).subscribe((data) => {
      console.log(data);
      this._getConversation(false, 'name').subscribe()
      this.navCtrl.getPrevious().data.isRead = true
      return data
    })
  }
  ngOnDestroy() {
    this._subscription.unsubscribe()
  }

}
// this.navCtrl.getPrevious().data.isDelete = false