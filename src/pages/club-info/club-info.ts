import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, MenuController, normalizeURL, LoadingController, Loading, AlertController, Content } from 'ionic-angular';
import { MembersPage } from '../members/members';
import { ConversationListPage } from '../conversation-list/conversation-list';
import { RequestService } from '../../services/request.service';
import { Clubs, Conversation } from '../../types/types';
import { map, catchError } from 'rxjs/operators';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { Subscription } from 'rxjs';
import { of } from 'rxjs/observable/of';
import { AppService } from '../../services/app.service';

/**
 * Generated class for the ClubInfoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-club-info',
  templateUrl: 'club-info.html',
})
export class ClubInfoPage {
  public id: number;
  public club: Clubs;
  private _subscription: Subscription
  public unreadMessageCount: number;
  public isRead: boolean = false;
  public isError: boolean = false;
  public isGet: boolean = false;
  @ViewChild(Content) content
  constructor(public navCtrl: NavController,
    private _requestService: RequestService,
    public navParams: NavParams,
    public menuCtrl: MenuController,
    private _loadCtrl: LoadingController,
    private _appService: AppService,
    private _alertCtrl: AlertController
  ) {
    this.id = this.navParams.get('id');
    this.isRead = this.navParams.get('isRead') || null;
  }
  ngOnInit() {
    this._combineObservable()
  }
  ionViewDidEnter() {
    this.isRead = this.navParams.get('isRead');
    console.log(this.isRead);
    if (this.isRead) {
      this._getConversation(false, 'name').subscribe()
    }
  }

  public joinClub() {
    this._requestService.joinClub(this.id).subscribe((data) => {
      console.log(data);
      this._confirm()
    })
  }
  private _confirm() {
    let alert = this._alertCtrl.create({
      message: 'Are you sure you want to join club?',
      cssClass: 'buttonCss',
      buttons: [
        {
          text: 'Yes',
          handler: () => {
            this.isError = false
            this._getConversation(false, 'name').subscribe()

          }
        },
        {
          text: 'No',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },

      ]
    })
    alert.present()
  }
  private _combineObservable() {
    let loading = this._loadCtrl.create()
    loading.present()
    const comine = forkJoin(
      this._getClubsById(),
    )
    this._subscription = comine.subscribe((data: any) => {
      if (data[0].status == 200) {
        this._getConversation(false, 'name').subscribe()
      } else {
        if (data[0] && data[0].status == 403) {
          this.isError = true
        }
      }
      this.isGet = true;
      loading.dismiss()
    },
      (error) => {
        console.log(error);
        if (error[0] && error[0].status == 403) {
          loading.dismiss()
          this.isGet = true
          this.isError = true
        }
      })
  }
  private _getConversation(isSearch: boolean, name: string) {
    return this._requestService.getConversationsByClubId(isSearch, name, this.id).pipe(
      map((data: { messages: Array<Conversation>, status: string, sum: number }) => {
        console.log(data);
        this.unreadMessageCount = data.sum;
        return data
      }))
  }
  private _getClubsById() {
    return this._requestService.getClubsById(this.id).pipe(
      map((data: { messages: Clubs[], status: string }) => {
        console.log(data);
        this.club = data["messages"][0];
        return data
      }))
  }
  public openMembersList() {
    this.navCtrl.push(MembersPage, { clubId: this.id, clubName: this.club.title })
  }
  public openConversationList() {
    this.navCtrl.push(ConversationListPage, { clubId: this.id, clubName: this.club.title })
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad ClubInfoPage');
  }
  ngOnDestroy() {
    this._subscription.unsubscribe()
  }

}
