import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, App, ViewController, Content, Platform, LoadingController } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { RequestService } from '../../services/request.service';

@Component({
  selector: 'page-registration',
  templateUrl: 'registration.html',
})
export class RegistrationPage {
  @ViewChild(Content) content: Content;
  public registrFormGroup: FormGroup;
  public message: string;
  public errorMessage: string
  public isError: boolean = false
  constructor(public navCtrl: NavController, private _loadingCtrl: LoadingController, private _requestService: RequestService, private _platform: Platform, private _viewCtrl: ViewController, public app: App, public navParams: NavParams, private _formBuilder: FormBuilder) {
    this._validation()
  }
  scrollTo(elementId: string) {
    if (this._platform.is('ios')) {
      let y = document.getElementById(elementId).offsetTop;
      this.content.scrollTo(0, y);
    }
  }
  private _validation() {
    this.registrFormGroup = this._formBuilder.group({
      name: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      phone: ['', Validators.required],
      company: ['', Validators.required],
      message: ['', Validators.required]
    })
  }
  public dismiss() {
    this._viewCtrl.dismiss()
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad RegistrationPage');
  }
  public joinClub() {
    let loading = this._loadingCtrl.create();
    loading.present();
    this._requestService.registration(this.registrFormGroup.value.name,
      this.registrFormGroup.value.email, this.registrFormGroup.value.company,
      this.registrFormGroup.value.phone, this.registrFormGroup.value.message)
      .subscribe((data) => {
        loading.dismiss();
        console.log(data);
        this._viewCtrl.dismiss(true)
      },
        (error) => {
          console.log(error);
          this.isError = true;
          let message: any = JSON.parse(error.error)
          let key = Object.keys(message)[0]
          console.log(key);
          if (key) {
            this.errorMessage = message[key][0];
            console.log(JSON.parse(error.error));
          } else {
            this.errorMessage = 'Invalid registration';
          }
          console.log(this.errorMessage);
          loading.dismiss();
        }
      )

    //this.app.getRootNav().setRoot(MenuPage);
  }

}
