import { Component, Inject } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { MemberInfoPage } from '../member-info/member-info';
import { RequestService } from '../../services/request.service';
import { Member } from '../../types/types';

/**
 * Generated class for the MembersPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-members',
  templateUrl: 'members.html',
})
export class MembersPage {
  public clubId: number;
  public clubName: string;
  public members: Array<Member>;
  public defaultImage;
  constructor(public navCtrl: NavController,
    @Inject('FILE_URL') public fileUrl,
     public navParams: NavParams,
      private _requestService: RequestService
      ) {
    this.clubId = this.navParams.get("clubId");
    this.clubName = this.navParams.get("clubName");    
  }
  ngOnInit() {
    this._getAllMembersOfClub(false, 'name')
  }
  public openMemberInfo(member:Member) {
    this.navCtrl.push(MemberInfoPage, { clubName: this.clubName, memberId: member.memberId,memberName:member.userName })
  }
  private _getAllMembersOfClub(isSearch: boolean, name: string) {
    this._requestService.getAllMembersByClubId(isSearch, name, this.clubId).subscribe((data: { members: Array<Member> }) => {
      console.log(data);
      this.members = data.members;
    })
  }
  public filter(event: string) {
    console.log(event);
    if (event) {
      this._getAllMembersOfClub(true, event)
    }
  }
  empty(ev) {
    this._getAllMembersOfClub(false, 'name')
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad MembersPage');
  }

}
