import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';
import { ClubInfoPage } from '../club-info/club-info';
import { RequestService } from '../../services/request.service';
import { Clubs } from '../../types/types';


@Component({
  selector: 'page-club',
  templateUrl: 'club.html',
})
export class ClubPage {
  public clubs: Array<Clubs> = [];
  public isClicked: boolean = false;
  constructor(public navCtrl: NavController, private _loadingCtrl: LoadingController, private _requestService: RequestService, public navParams: NavParams) {
  }
  ngOnInit() {
    this._getAllClubs(false, 'name')
  }

  private _getAllClubs(isSearch: boolean, name: string) {
    let loadCtrl = this._loadingCtrl.create();
    loadCtrl.present()
    this._requestService.getClubs(isSearch, name).subscribe((data: { messages: Array<Clubs>, status: string }) => {
      console.log(data);
      loadCtrl.dismiss()
      this.clubs = data.messages;
    })
  }
  public filter(event: string) {
    console.log(event);
    if (event) {
      this._getAllClubs(true, event)
    }
  }
  empty(ev) {
    this._getAllClubs(false, 'name')
  }
  public openClubInfo(club: Clubs) {
    this.navCtrl.push(ClubInfoPage, { id: club.id })
  }
}
