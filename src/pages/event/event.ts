import { Component, Inject } from '@angular/core';
import { NavController, NavParams, LoadingController, ModalController } from 'ionic-angular';
import { EventInfoPage } from '../event-info/event-info';
import { RequestService } from '../../services/request.service';
import { CreateEventPage } from '../../modals/barrel';
import { NewEvent } from '../../types/types';


@Component({
  selector: 'page-event',
  templateUrl: 'event.html',
})
export class EventPage {
  public events: Array<NewEvent> = [];
  defaultImage = "assets/imgs/colors.jpg"
  public isDelete: boolean = false;
  public id: number
  constructor(
    public navCtrl: NavController,
    @Inject('FILE_URL') public fileUrl,
    private _modalCtrl: ModalController,
    private _requestService: RequestService,
    private _loadingCtrl: LoadingController,
    public navParams: NavParams) {
  }
  public openEvent(event: NewEvent) {
    this.navCtrl.push(EventInfoPage, { id: event.id, name: event.name });
  }
  ionViewWillEnter() {
    this.isDelete = this.navParams.get('isDelete') || null;
    if (this.isDelete) {
      console.log('delete');
      this._getEvents(false, 'name')
    }
  }
  ngOnInit() {
    this._getEvents(false, 'name')
  }
  public setImage(event: NewEvent) {
    let styles = {}
    if (event && event.image) {
      styles["background-image"] = 'url(' + this.fileUrl + event.image + ')'

    } else {
      styles["background-image"] = 'url(' + this.defaultImage + ')'
    }
    return styles;
  }
  private _getEvents(isSearch: boolean, name: string) {
    let loadCtrl = this._loadingCtrl.create();
    loadCtrl.present()
    this._requestService.getEvents(isSearch, name).subscribe((data: { messages: Array<NewEvent>, status: string }) => {
      console.log(data);
      this.events = data["messages"]
      loadCtrl.dismiss()
    })
  }
  public filter(event: string) {
    console.log(event);
    if (event) {
      this._getEvents(true, event)
    }
  }
  empty(ev) {
    this._getEvents(false, 'name')
  }
  public createEvent() {
    let modal = this._modalCtrl.create(CreateEventPage);
    modal.onDidDismiss((data) => {
      if (data) {
        this._getEvents(false, 'name')
      }
    })
    modal.present()
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad EventPage');
  }

}
