import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, Tabs } from 'ionic-angular';
import { ClubPage } from '../club/club';
import { EventPage } from '../event/event';
import { UserProfilePage } from '../user-profile/user-profile';
import { SettingsPage } from '../settings/settings';
import { LanguagesPage } from '../languages/languages';
import { isRightSide } from 'ionic-angular/umd/util/util';

@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html',
})
export class TabsPage {
  @ViewChild('myTabs') tabRef: Tabs;
  tabs = [
    { title: "Club", isShow: true, root: ClubPage },
    { title: "Event", isShow: true, root: EventPage },
    { title: "User profil", isShow: false, root: UserProfilePage },
    { title: "Language", isShow: false, root: LanguagesPage },
    { title: "Settings", isShow: false, root: SettingsPage },
  ];

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TabsPage');
  }

}
