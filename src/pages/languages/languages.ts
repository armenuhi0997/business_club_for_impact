import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController } from 'ionic-angular';


@Component({
  selector: 'page-languages',
  templateUrl: 'languages.html',
})
export class LanguagesPage { 
  public isOpen: boolean = true
  constructor(public navCtrl: NavController,public menuCtrl:MenuController, public navParams: NavParams) {
  }
  public openLanguagesList(){
    this.isOpen=!this.isOpen
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad LanguagesPage');
  }

}
