import { Component, ViewChild, Inject } from '@angular/core';
import { NavController, NavParams, Nav, MenuController, AlertController } from 'ionic-angular';
import { TabsPage } from '../tabs/tabs';
import { LoginPage } from '../barrel';
import { RequestService } from '../../services/request.service';
import { EnterUser } from '../../types/types';
import { MenuEntityService } from '../../services/menu-entity.service';


/**
 * Generated class for the MenuPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-menu',
  templateUrl: 'menu.html',
})
export class MenuPage {
  @ViewChild(Nav) nav: Nav;
  rootPage = TabsPage;
  public userImage = 'assets/imgs/colors.jpg'
  public userName: string;
  public pages = [
    {
      title: 'User profil',
      page: "UserProfilePage",
      pageName: "TabsPage",
      tabIndex: 2,
      icon: 'assets/imgs/person_icon.png'
    },
    // {
    //   title: 'Languages',
    //   page: "LanguagesPage",
    //   pageName: "TabsPage",
    //   tabIndex: 3,
    //   icon: 'assets/imgs/language_icon.png'
    // },
    {
      title: 'Settings',
      page: "SettingsPage",
      pageName: "TabsPage",
      tabIndex: 4,
      icon: 'assets/imgs/settings_icon.png'
    }
  ]
  constructor(public navCtrl: NavController,
    private _alertCtrl: AlertController,
    public navParams: NavParams,
    private _menuCtrl: MenuController,
    private _requestService: RequestService,
    public menuEntityService: MenuEntityService,
    @Inject('FILE_URL') public fileUrl:string
  ) { }

  ionViewWillEnter() {
    this._getUser()
  }
  public openPage(page) {
    this.nav.getActiveChildNavs()[0].select(page.tabIndex);
  }
  private _getUser() {
    this._requestService.getUser().subscribe((data: { user: EnterUser }) => {
      console.log(data);
      this.menuEntityService.name = data.user.name;
      this.menuEntityService.image = "url(" + this.fileUrl + data.user.avatar + " )"
    })
  }
  public exit() {
    let alert = this._alertCtrl.create({
      message: 'Are you sure you want to leave?',
      cssClass: 'buttonCss',
      buttons: [
        {
          text: 'Yes',
          handler: () => {
            this._menuCtrl.enable(false, 'authenticated');
            localStorage.removeItem('accessToken')
            this.navCtrl.setRoot(LoginPage)
          }
        },
        {
          text: 'No',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },

      ]
    })
    alert.present()

  }


}
