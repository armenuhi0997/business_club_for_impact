import { Component, Inject } from '@angular/core';
import { NavController, NavParams, ModalController, ViewController, LoadingController, AlertController } from 'ionic-angular';
import { InvitePage, CreateEventPage } from '../../modals/barrel';
import { RequestService } from '../../services/request.service';
import { NewEvent, EnterUser } from '../../types/types';
import { map } from 'rxjs/operators';
import { Subscription } from 'rxjs';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { AppService } from '../../services/app.service';

/**
 * Generated class for the EventInfoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-event-info',
  templateUrl: 'event-info.html',
})
export class EventInfoPage {
  public timeFormat: string;
  public name: string;
  public id: number;
  public eventInfo: NewEvent;
  public defaultImage = "assets/imgs/colors.jpg";
  public isGet: boolean = false
  public startTimeFormat: string;
  public endTimeFormat: string;
  private _subription:Subscription;
  public createrEventId:number;
  public userId:number;
  constructor(
    public navCtrl: NavController,
    private _viewCtrl: ViewController,
    private _requestService: RequestService,
    public navParams: NavParams,
    private _modalCtrl: ModalController,
    private _loadCtrl: LoadingController,
    private _alertCtrl: AlertController,
    @Inject('FILE_URL') public fileUrl,
    private _appService:AppService
  ) {
    this.name = navParams.get("name");
    this.id = this.navParams.get("id")
  }
  ngOnInit() {
    this.navCtrl.getPrevious().data.isDelete = false;
    this._combineObservable()
  }
  private _combineObservable(){
    const combine=forkJoin(
      this._getEvent(),
      this._getUser()
    )
    this._subription=combine.subscribe()
  }
  public invite() {
    let modal = this._modalCtrl.create(InvitePage, { id: this.eventInfo.id });
    modal.present()
  }

  public createEvent() {
    let modal = this._modalCtrl.create(CreateEventPage, { id: this.eventInfo.id });
    modal.onDidDismiss((data) => {
      if (data) {
        this._getEvent().subscribe();
        this.navCtrl.getPrevious().data.isDelete = true;
      }
    })
    modal.present()
  }
  private _getUser() {
   return  this._requestService.getUser().pipe(
      map((data: { user: EnterUser })=>{
        this.userId=data.user.id
        return data
      })
    )

  }
  private _getEvent() {
    let loading = this._loadCtrl.create()
    loading.present()
  return  this._requestService.getEventById(this.id).pipe(
      map((data: { 'messages': Array<NewEvent>, 'status': string })=>{
        console.log(data);
        loading.dismiss();
        
        this.eventInfo = data.messages[0];
        this.createrEventId=this.eventInfo.createrId
        this.name = this.eventInfo.name
        if (this.eventInfo.image) {
          this.defaultImage = "url(" + this.fileUrl + this.eventInfo.image + ")";
        } else {
          this.defaultImage = "url(" + this.defaultImage + ")";
        }
        let newStartDateIndex = this.eventInfo["start-time"].indexOf(' ');
        let newEndDateIndex = this.eventInfo["end-time"].indexOf(' ');
        // let startTimeArr = this.eventInfo["start-time"].substr(0,newStartDateIndex).split('-');
        // this.startTime=this.eventInfo["start-time"].substr(newStartDateIndex+1)
  
        let startTimeArr = this.eventInfo["start-time"].substr(0, newStartDateIndex).split('-');
        let endTimeArr = this.eventInfo["end-time"].substr(0, newEndDateIndex).split('-');
        console.log(startTimeArr);
        console.log(endTimeArr);
        
        //2019-02-12";
        if (startTimeArr[2])
          startTimeArr[2] = startTimeArr[2].length == 1 ? '0' + startTimeArr[2] : startTimeArr[2];
        if (startTimeArr[1])
          startTimeArr[1] = startTimeArr[1].length == 1 ? '0' + startTimeArr[1] : startTimeArr[1];
  
        this.startTimeFormat = startTimeArr[2] + ',' + startTimeArr[1] + ',' + startTimeArr[0] +
          ' at ' + this.eventInfo["start-time"].substr(newStartDateIndex + 1);
        if (endTimeArr[2])
          endTimeArr[2] = endTimeArr[2].length == 1 ? '0' + endTimeArr[2] : endTimeArr[2]
        if (endTimeArr[1])
          endTimeArr[1] = endTimeArr[1].length == 1 ? '0' + endTimeArr[1] : endTimeArr[1]
  
        this.endTimeFormat = endTimeArr[2] + ',' + endTimeArr[1] + ',' + endTimeArr[0] +
          ' at ' + this.eventInfo["end-time"].substr(newEndDateIndex + 1);
  
        let count = 0
        for (let i = 0; i < 3; i++) {
          if (startTimeArr[i] == endTimeArr[i]) {
            count++
          }
        }
        if (count == 3) {
          this.timeFormat = this.startTimeFormat + '-' + this.eventInfo["end-time"].substr(newEndDateIndex + 1);
        } else {
          this.timeFormat = this.startTimeFormat + '-' + this.endTimeFormat
        }
        console.log(this.startTimeFormat);
  
        this.isGet = true;
        return data
      })
    )
  
  }
  public deleteEvent() {
    let alert = this._alertCtrl.create({
      message: 'Are you sure you want to delete event?',
      cssClass: 'buttonCss',
      buttons: [
        {
          text: 'Yes',
          handler: () => {
            this._requestService.deleteEvent(this.id).subscribe((data) => {
              console.log(data);
              this.navCtrl.getPrevious().data.isDelete = true;
              this.navCtrl.pop();
            })
          }
        },
        {
          text: 'No',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },

      ]
    })
    alert.present()
  }
  public doActionEvent(url){
    this._requestService.addActionEvent(url,this.id).subscribe((data)=>{
      console.log(data);
      this._getEvent().subscribe()  
      this._appService.getEvents() 
    })
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad EventInfoPage');
  }
  ngOnDestroy(){
    this._subription.unsubscribe()
  }

}
