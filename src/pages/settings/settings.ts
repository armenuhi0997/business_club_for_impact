import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, Platform, Content, MenuController, ModalController } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { EnterUser } from '../../types/types';
import { RequestService } from '../../services/request.service';
import { MenuEntityService } from '../../services/menu-entity.service';
import { ChangePasswordModal } from '../../modals/barrel';


@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
})
export class SettingsPage {
  @ViewChild(Content) content: Content
  public formGroup: FormGroup;
  public usersEntity: EnterUser;
  public name;
  public email;
  public phone;
  public company;
  public isError: boolean = false
  constructor(public navCtrl: NavController,
    private _menuEntityService: MenuEntityService,
    private _requestService: RequestService,
    public navParams: NavParams,
    private _formBuilder: FormBuilder,
    private _modalCtrl: ModalController
  ) {
  }
  scrollTo(elementId: string) {
    let y = document.getElementById(elementId).offsetTop;
    this.content.scrollTo(0, y);
  }
  ngOnInit() {
    this.validation()
  }
  ionViewWillEnter() {
    console.log('ionViewDidLoad SettingsPage');
    this._getUser()
  }
  ionViewDidLoad() {

  }

  private _getUser() {
    this._requestService.getUser().subscribe((data: { user: EnterUser }) => {
      console.log(data);
      this.usersEntity = data.user;
      this.name = this.usersEntity.name;
      this.phone = this.usersEntity.phone;
      this.company = this.usersEntity.company;
      this.email = this.usersEntity.email;
    })
  }
  validation() {
    this.formGroup = this._formBuilder.group({
      name: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      company: ['', Validators.required],
      phone: ['', Validators.required]
    })
  }
  public save() {
    if (this.formGroup.invalid) {
      this.isError = true
    } else {
      this.isError = false
      this._requestService.changeUser(this.name, this.company, this.phone, this.email).subscribe((data) => {
        console.log(data);
        this._menuEntityService.name = this.name
        this.dismiss()
      })
    }
  }
  dismiss() {
    this.navCtrl.parent.select(0);
  }
  public changePassword() {
    let modal = this._modalCtrl.create(ChangePasswordModal);
    modal.present()
  }

}
