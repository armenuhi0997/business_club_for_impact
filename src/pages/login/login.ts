import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, App, ModalController, Keyboard, Platform, Content, LoadingController } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MenuPage } from '../menu/menu';
import { RegistrationPage } from '../registration/registration';
import { RequestService } from '../../services/request.service';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  @ViewChild(Content) content: Content;
  public loginFormGroup: FormGroup;
  public isError: boolean = false;
  public errorMessage: string;
  constructor(public navCtrl: NavController, private _loadingCtrl: LoadingController, private _requestService: RequestService, private _platform: Platform, public platform: Platform, public keyboard: Keyboard, public app: App, private _modalCtrl: ModalController, public navParams: NavParams, private _formBuilder: FormBuilder) {
    this._validation()
  }
  scrollTo(elementId: string) {
    let y = document.getElementById(elementId).offsetTop
    this.content.scrollTo(0, y, 2000);
  }
  private _validation() {
    this.loginFormGroup = this._formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required]
    })
  }
  public login() {
    if (this.loginFormGroup.valid) {
      this.isError = false
      let loading = this._loadingCtrl.create();
      loading.present();
      this._requestService.login(this.loginFormGroup.value.email, this.loginFormGroup.value.password).subscribe((data: { token: string }) => {
        localStorage.setItem('accessToken', JSON.stringify(data.token));        
        loading.dismiss();
        this.navCtrl.setRoot(MenuPage)
      },
        (error) => {          
          loading.dismiss();
          this.isError = true
          this.errorMessage = 'Invalid login attempt'
        })
    } else {
      this.isError = true
      this.errorMessage = 'Please,fill all fields'
    }
    //  this.app.getRootNav().setRoot(MenuPage);

  }
  public openRegistrationPage() {
    let modal = this._modalCtrl.create(RegistrationPage)
    modal.present()
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

}
