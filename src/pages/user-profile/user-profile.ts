import { Component, Inject } from '@angular/core';
import { NavController, NavParams, MenuController, ActionSheetController, Header, normalizeURL, LoadingController, ToastController } from 'ionic-angular';
import { AppService } from '../../services/app.service';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { FileTransferObject, FileTransfer, FileUploadOptions } from '@ionic-native/file-transfer';
import { RequestService } from '../../services/request.service';
import { EnterUser } from '../../types/types';
import { File } from '@ionic-native/file';
import { HttpClient } from '@angular/common/http';
import { ApiService } from '../../services/api.service';
import { MenuEntityService } from '../../services/menu-entity.service';

/**
 * Generated class for the UserProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-user-profile',
  templateUrl: 'user-profile.html',
})
export class UserProfilePage {
  public isClick = false;
  public lastImage = "assets/colors.jpg";
  public entityUser: EnterUser;
  public noteImage: string;
  public type: number;
  constructor(
    public navCtrl: NavController,
    public appService: AppService,
    public navParams: NavParams,
    public menuCtrl: MenuController,
    private _actionsheetCtrl: ActionSheetController,
    private _camera: Camera,
    private _transfer: FileTransfer,
    private _file: File,
    private _httpClient: HttpClient,
    private _apiService: ApiService,
    private _requestService: RequestService,
    private _loadCtrl: LoadingController,
    private _toastCtrl: ToastController,
    private _menuEntityService:MenuEntityService,
    @Inject('BASE_URL') private _baseUrl,
    @Inject('FILE_URL') public fileUrl
  ) { }
  dismiss() {
    this.navCtrl.parent.select(0);
  }
  ionViewDidEnter() {
    console.log('ionViewDidLoad UserProfilePage');
    this._getUser()
  }
  private _getUser() {
    this._requestService.getUser().subscribe((data: { user: EnterUser }) => {
      console.log(data);
      this.entityUser = data.user
      this.lastImage = "url(" + this.fileUrl + data.user.avatar + " )"
    })
  }

  getPhoto() {
    let actionSheet = this._actionsheetCtrl.create({
      cssClass: 'action-sheets-basic-page',
      buttons: [
        {
          text: 'Take photo',
          role: 'destructive',
          handler: () => {
            this.getImage(1);
          }
        },
        {
          text: 'Upload photo',
          handler: () => {
            this.getImage(0);
          }
        },
      ]
    });
    actionSheet.present();
  }
  getImage(type) {
    this.type = type
    const options: CameraOptions = {
      quality: 100,
      destinationType: this._camera.DestinationType.FILE_URI,
      sourceType: type,
      mediaType: this._camera.MediaType.PICTURE,
      correctOrientation: true,
      allowEdit: true
    }

    this._camera.getPicture(options).then((imageData) => {
      this.noteImage = imageData;
      this.uploadFile()
    }, (err) => {
      console.log(err);
      // this.presentToast(err);
    });
  }

  uploadFile() {
    let loader = this._loadCtrl.create();
    loader.present();
    const fileTransfer: FileTransferObject = this._transfer.create();
    let accessToken = JSON.parse(localStorage.getItem('accessToken')) || '';
    let options: FileUploadOptions = {
      fileKey: 'photo',
      fileName: 'ionicfile.jpg',
      chunkedMode: false,
      mimeType: "multipart/form-data",
      httpMethod: 'post',
      headers: {
        'Authorization': 'Bearer ' + accessToken
      }
    }

    fileTransfer.upload(this.noteImage, this._baseUrl+'user/upload', options)
      .then((data) => {
        console.log(data + " Uploaded Successfully");
        loader.dismiss();
        this.presentToast("Image uploaded successfully");
        this._menuEntityService.image="url("+this.noteImage+")"
        this._getUser()
      }, (err) => {
        console.log(err);
        loader.dismiss();
        this.presentToast('Something went wrong try again');
      });
  }
  presentToast(msg) {
    let toast = this._toastCtrl.create({
      message: msg,
      duration: 6000,
      position: 'bottom'
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }
  sendEvent() {
    this.appService.isCheck = !this.appService.isCheck
  }
}
