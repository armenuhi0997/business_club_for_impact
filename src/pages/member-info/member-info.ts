import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { RequestService } from '../../services/request.service';
import { User } from '../../types/types';

/**
 * Generated class for the MemberInfoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-member-info',
  templateUrl: 'member-info.html',
})
export class MemberInfoPage {
  public clubName: string;
  public memberId: number;
  public member: User;
  public memberName:string
  info = ""
  constructor(public navCtrl: NavController, private _requestService: RequestService, public navParams: NavParams) {
    this.clubName = this.navParams.get("clubName");
    this.memberId = this.navParams.get("memberId")  
    this.memberName=this.navParams.get("memberName")  
  }
  ngOnInit() {
    this._getMember()
  }
  private _getMember() {
    this._requestService.getMemberById(this.memberId).subscribe((data: { user: Array<User> }) => {
      console.log(data);
      this.member = data.user[0]
    })
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad MemberInfoPage');
  }

}
