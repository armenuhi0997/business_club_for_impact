import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { LoginPage, TabsPage } from '../pages/barrel';
import { RequestService } from '../services/request.service';
import { MenuPage } from '../pages/menu/menu';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage: any;

  constructor(platform: Platform, statusBar: StatusBar, private _requestService: RequestService, splashScreen: SplashScreen) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }
  ngAfterViewInit() {
    if (!(JSON.parse(localStorage.getItem('accessToken')))) {      
      this.rootPage = LoginPage
    } else {
      this._requestService.checkAccessToken().subscribe((data: any) => {
        console.log(data);
        if (data.status == 200) {
          this.rootPage = MenuPage
        }
      },
        (error) => {
          console.log(error);
          if (error.status == 401) {
            this._requestService.getNewToken().subscribe((data: any) => {
              console.log(data);
              localStorage.setItem('accessToken', JSON.stringify(data.new_token))
              this.rootPage = MenuPage
            },
              (error) => {
                console.log(error);
                localStorage.setItem('accessToken', JSON.stringify(error.new_token))
                this.rootPage = MenuPage
              })
          } else {

            if (error.status == 404) {
              this.rootPage = LoginPage
            } else {
              this.rootPage = LoginPage
            }

          }
        })
    }
  }
}