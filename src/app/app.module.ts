import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { HttpClientModule } from '@angular/common/http';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { File } from '@ionic-native/file';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { FileChooser } from '@ionic-native/file-chooser';
import { FileOpener } from '@ionic-native/file-opener';
import { DocumentViewer, DocumentViewerOptions } from '@ionic-native/document-viewer'
import {
  LoginPage,
  RegistrationPage,
  TabsPage,
  ClubPage,
  EventPage,
  UserProfilePage,
  LanguagesPage,
  SettingsPage,
  ClubInfoPage,
  MemberInfoPage,
  MembersPage,
  ConversationListPage,
  EventInfoPage,
} from '../pages/barrel';
import { MenuPage } from '../pages/menu/menu';
import {
  MoreInfoComponent,
  NewHeaderComponent,
  SearchPage,
  ModalsHeaderComponent,
  AlertEventsComponent
} from '../components/barrel';
import { ConversationModal, CreateConversationPage, InvitePage, CreateEventPage, ChangePasswordModal } from '../modals/barrel';
import { ApiService } from '../services/api.service';
import { RequestService } from '../services/request.service';
import { Camera } from '@ionic-native/camera';
import { Crop } from '@ionic-native/crop';
import { AppService } from '../services/app.service';
import { DatePipe } from '@angular/common';
import { FilePath } from '@ionic-native/file-path'
import { MenuEntityService } from '../services/menu-entity.service';
import { httpParams, fileParams } from '../params/http.params';
import { MorePointPipe } from '../pipes/more-point.pipe';
@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    RegistrationPage,
    TabsPage,
    MenuPage,
    ClubPage,
    EventPage,
    UserProfilePage,
    LanguagesPage,
    SettingsPage,
    ClubInfoPage,
    MoreInfoComponent,
    MemberInfoPage,
    MembersPage,
    NewHeaderComponent,
    ConversationListPage,
    SearchPage,
    ConversationModal,
    CreateConversationPage,
    ModalsHeaderComponent,
    EventInfoPage,
    InvitePage,
    CreateEventPage,
    AlertEventsComponent,
    ChangePasswordModal,
    MorePointPipe
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp,
      {
        mode: 'md'
      })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    RegistrationPage,
    TabsPage,
    MenuPage,
    ClubPage,
    EventPage,
    UserProfilePage,
    LanguagesPage,
    SettingsPage,
    ClubInfoPage,
    MoreInfoComponent,
    MemberInfoPage,
    MembersPage,
    NewHeaderComponent,
    ConversationListPage,
    SearchPage,
    ConversationModal,
    CreateConversationPage,
    ModalsHeaderComponent,
    EventInfoPage,
    InvitePage,
    CreateEventPage,
    AlertEventsComponent,
    ChangePasswordModal
  ],
  providers: [
    StatusBar,
    SplashScreen,
    ApiService,
    AppService,
    MenuEntityService,
    RequestService,
    Camera,
    Crop,
    File,
    FileTransfer,
    FileTransferObject,
    FileChooser,
    FilePath,
    DatePipe,
    FileOpener,
    DocumentViewer,
    { provide: 'BASE_URL', useValue: httpParams.baseUrl },
    { provide: 'FILE_URL', useValue: fileParams.fileUrl },
    { provide: ErrorHandler, useClass: IonicErrorHandler }
  ]
})
export class AppModule { }
