import { Component, Input } from "@angular/core";
import { ViewController } from "ionic-angular";

@Component({
    selector:'modals-header',
    templateUrl:'modals-header.component.html',

})
export class ModalsHeaderComponent{
    @Input() title;
    constructor(private _viewCtrl:ViewController){}
    public dismiss(){
        this._viewCtrl.dismiss()
    }
}