import { Component, Input, Output, EventEmitter, Inject } from "@angular/core";
import { AppService } from "../../services/app.service";
import { RequestService } from "../../services/request.service";
import { NewEvent } from "../../types/types";
import { NavController, App } from "ionic-angular";
import { EventInfoPage } from "../../pages/barrel";

@Component({
    selector: 'alert-event',
    templateUrl: 'alert-events.component.html',
})
export class AlertEventsComponent {
    @Input() isClick;
    @Input() top;
    @Output() id = new EventEmitter();
   
    public events: NewEvent[] = [];
    constructor(public appService: AppService, public app: App, public navCtrl: NavController,
        private _requestService: RequestService,
        @Inject('FILE_URL') public fileUrl) {
      
            
         }
    ngOnInit() {        
        this.appService.getEvents()
    }
    close() {
        this.appService.isCheck = false;
    }

    public openEvent(event: NewEvent) {
        this.appService.isCheck = false;
        
        this.navCtrl.parent.select(1)
            .then(() => this.navCtrl.parent.getSelected().push(EventInfoPage, { id: event.id }))
    }
}