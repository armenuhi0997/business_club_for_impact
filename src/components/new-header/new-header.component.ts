import { Component, Input, Output, EventEmitter, ViewChild } from "@angular/core";
import { ViewController, NavController, MenuController, Navbar } from "ionic-angular";
import { AppService } from "../../services/app.service";
import { RequestService } from "../../services/request.service";
import { NewEvent } from "../../types/types";

@Component({
    selector: 'new-header',
    templateUrl: 'new-header.component.html',
})
export class NewHeaderComponent {
    @Input() title: string;
    @Input() isLanguage;
    @Input() isDismiss;
    @Input() isTab;
   // @Output() sendHeight = new EventEmitter()
   // @ViewChild('navbar') navbar
    public eventCount: number
    public logo: string = 'assets/imgs/business_club_for_impact_logo.png'
    constructor(
        public navCtrl: NavController,
        public appservice: AppService,
        public menuCtrl: MenuController,
    ) {
        // setTimeout(() => {
        //    this.sendHeight.emit(this.navbar._elementRef.nativeElement.offsetHeight)
        //     console.log(this.navbar._elementRef.nativeElement.offsetHeight, ' navbar');
        // })
    }

    dismiss() {
        if (this.isLanguage) {
            this.navCtrl.parent.select(0)
        } else {
            this.navCtrl.pop()
        }
    }

    sendEvent() {
        this.appservice.isCheck = !this.appservice.isCheck
    }
}