import { Component, Input, Inject } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Title } from '@angular/platform-browser';

/**
 * Generated class for the MoreInfoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-more-info',
  templateUrl: 'more-info.html',
})
export class MoreInfoComponent {
  @Input() title: string;
  @Input() text: string
  @Input() radius: string
  @Input() height: string;
  @Input() width: string;
  @Input() greatness: string;
  @Input() avatar: string;

  constructor(public navCtrl: NavController, public navParams: NavParams,@Inject('FILE_URL') public fileUrl) {
  }
  styleObject(): Object {
    return { height: this.height + 'px', backgroundImage: 'url(' + this.fileUrl + this.avatar + ')', width: this.width + 'px', borderRadius: this.radius + this.greatness }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MoreInfoPage');
  }

}
