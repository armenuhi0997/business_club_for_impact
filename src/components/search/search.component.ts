import { Component, Output, EventEmitter, Input } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';


@Component({
  selector: 'search',
  templateUrl: 'search.component.html',
})
export class SearchPage {
  @Input() noPadding;
  @Input() array
  @Output() val = new EventEmitter()
  @Output() empty = new EventEmitter()
  public myInput: string
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }
  public sendName() {
    if (this.myInput && this.myInput.trim() !== '')
      this.val.emit(this.myInput)
  }
  public sendEmptyVal(event) {
    if (event && this.myInput == '') {
      this.empty.emit(this.myInput)
    }
  }
}
