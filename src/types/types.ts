export interface Clubs {
    created_at: string
    footer_text: string
    id: number
    slider: any
    text: string
    title: string
    updated_at: string
}
export interface Member {
    userImage: string
    userName: string,
    memberId: number,
    isSelect?: boolean
}
export interface User {
    avatar: string
    name: string
    id?: number
    isSelect?: boolean
    eventId?: number,
    isDisable?: boolean
}
export interface Conversation {
    clubId: number
    created_at: string
    id: number
    name: string
    updated_at: string,
    count: number,
    lastMessage: LastMessage[],
    lastMessageImage: LasMessageImage[],
    conversationName: {
        userAvatar: string
        userName: string
    }[]
    image1?:string,
    image2?:string
}
export interface LastMessage {
    content: string
    conversationId: number
    created_at: string
    fileName: string
    id: number
    type: number
    updated_at: string
    userAvatar: string
    userId: number
    userName: string
}
export class LasMessageImage {
    content: string
    conversationId: 1
    created_at: string
    fileName: string
    id: number
    type: number
    updated_at: string
    userAvatar: string
    userId: number
    userName: string
}
export interface NewEvent {
    "address": string
    "created_at": string
    "createrId": number
    "description": string
    "end-time": string
    "id": number
    "image": string
    "name": string
    "start-time": string
    "updated_at": string
    "type": number
}
export class EnterUser {
    avatar: string
    company: string
    created_at: string
    email: string
    id: number
    message: string
    name: string
    phone: null
    role_id: number
    settings: null
    updated_at: string
}
export interface Message {
    avatar: string
    company: string
    content: string
    conversationId: number
    created_at: string
    email: string
    fileName: string
    id: number
    message: string
    name: string
    phone: string
    remember_token: null
    role_id: number
    settings: null
    type: number
    updated_at: string
    userId: number
}
//"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC8xOTIuMTY4LjAuMTE0OjgwMDBcL2FwaVwvbG9naW4iLCJpYXQiOjE1NTIzOTAxOTYsImV4cCI6MTU1MjM5Mzc5NiwibmJmIjoxNTUyMzkwMTk2LCJqdGkiOiJVb3lXTFN3MUhVRWFvcEJGIiwic3ViIjoxMywicHJ2IjoiODdlMGFmMWVmOWZkMTU4MTJmZGVjOTcxNTNhMTRlMGIwNDc1NDZhYSJ9.YhPWoeYXeJ1hHDRyPPGAEFuzBQe5qAiiy86S-kqhsPE"