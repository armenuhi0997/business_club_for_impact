import { Pipe, PipeTransform } from '@angular/core'
@Pipe({
    name: 'morePoint'
})
export class MorePointPipe implements PipeTransform {
    transform(val: string) {
        if (val.length > 150) {
            return val.substr(0, 150) + '...'
        } else {
            return val
        }
    }
}