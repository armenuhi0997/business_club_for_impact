import { Component, ViewChild, ElementRef, Inject } from '@angular/core';
import { NavController, NavParams, ViewController, Content, LoadingController } from 'ionic-angular';
import { RequestService } from '../../services/request.service';
import { Member, Conversation } from '../../types/types';

/**
 * Generated class for the CreateConversationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-create-conversation',
  templateUrl: 'create-conversation.html',
})
export class CreateConversationPage {
  // @ViewChild(Content) content: Content;
  @ViewChild('headerTag') headerTag: ElementRef;
  @ViewChild('scrollableTag') scrollableTag: ElementRef;
  private _isDissmiss: boolean = false;
  public selectedMemebersArr: Array<{ name: string, id: number, userImage: string }> = []
  public clubId: number;
  public clubName: string
  public allMembers: Array<Member> = [];
  constructor(public navCtrl: NavController,
    private _loadCtrl: LoadingController,
    private _requestService: RequestService,
    public navParams: NavParams,
    private _viewCtrl: ViewController,
    @Inject('FILE_URL') public fileUrl
  ) {
    this.clubId = this.navParams.get("clubId");
    this.clubName = this.navParams.get("clubName")
  }
  ngOnInit() {
    this._getMembers(false, 'name')
  }
  /**
   * 
   * @param isSearch 
   * @param name 
   */
  private _getMembers(isSearch: boolean, name: string) {
    this._requestService.getAllMembersByClubId(isSearch, name, this.clubId).subscribe((data: { members: Array<Member> }) => {
      console.log(data);
      this.allMembers = data["members"];
      for (let member of this.allMembers) {
        if (!this.selectedMemebersArr[0]) {
          member['isSelect'] = false
        } else {
          for (let select of this.selectedMemebersArr) {
            member['isSelect'] = select.id == member.memberId ? true : false
          }
        }
      }
      console.log(this.allMembers);
    })

  }
  ionViewDidEnter() {
    let interval = setInterval(() => {
      this.setMarginTop();
      if (this._isDissmiss) {
        clearInterval(interval)
      }
    }, 500)
  }
  setMarginTop() {
    let offset = this.headerTag.nativeElement.offsetHeight;
    (<HTMLDivElement>this.scrollableTag.nativeElement).style.marginTop = offset + 20 + 'px';
    // console.log((<HTMLDivElement>this.scrollableTag.nativeElement).style.marginTop);
  }
  public dismiss() {
    this._viewCtrl.dismiss();
    this._isDissmiss = true
  }
  /**
   * 
   * @param member 
   * @param strValue 
   */
  public selectMember(member: Member, strValue?: string) {
    if (strValue) {
      member.isSelect = !member.isSelect;
    }
    if (member.isSelect) {
      this.selectedMemebersArr.push({ name: member.userName, id: member.memberId, userImage: member.userImage })
    }
    else {
      let index = this.selectedMemebersArr.findIndex((data) => data.id === member.memberId);
      console.log(index);
      if (index !== -1)
        this.selectedMemebersArr.splice(index, 1)
    }
    console.log(this.selectedMemebersArr);
  }
  /**
   * 
   * @param i 
   */
  public delete(i: number) {
    let index = this.allMembers.findIndex(data => data.memberId === this.selectedMemebersArr[i].id);
    if (index !== -1) {
      this.allMembers[index].isSelect = false;
      this.selectedMemebersArr.splice(i, 1)
    }
  }
  public createConversation() {
    let loading = this._loadCtrl.create();
    loading.present()
    let name: string = '';
    let usersIdArr: Array<number> = []
    for(let i=0;i<this.selectedMemebersArr.length;i++){
      usersIdArr.push(this.selectedMemebersArr[i].id);
      if(i<3){
        name += this.selectedMemebersArr[i].name[0];
      }
    }
    console.log(name);
    console.log(usersIdArr);
    this._requestService.addConversation(name, this.clubId, usersIdArr).subscribe((data: { user: Conversation }) => {
      console.log(data);
      loading.dismiss()
      this._viewCtrl.dismiss({ name: name, id: data.user.id })
    })
  }
  /**
   * 
   * @param event 
   */
  public filter(event: string) {
    console.log(event);
    if (event) {
      this._getMembers(true, event);
    }
  }
  empty(ev) {
    this._getMembers(false, 'name')
  }
}
