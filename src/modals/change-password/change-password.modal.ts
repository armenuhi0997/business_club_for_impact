import { Component } from "@angular/core";
import { RequestService } from "../../services/request.service";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { ViewController } from "ionic-angular";

@Component({
    selector: 'change-password',
    templateUrl: 'change-password.modal.html',
})
export class ChangePasswordModal {
    public formGroup: FormGroup;
    public isError: boolean = false;
    public isTrue: boolean = false;
    public errorMessage: string;
    public isOld: boolean = false
    public isNew: boolean = false
    public isRepeat: boolean = false;
    public oldIcon = 'eye-off'
    public newIcon = 'eye-off'
    public repeatIcon = 'eye-off'
    public typeOld: string = "password"
    public typeNew: string = "password"
    public typeRepeat: string = "password"
    constructor(private _requestService: RequestService,
        private _formBuilder: FormBuilder,
        private _viewCtrl: ViewController) { }
    ngOnInit() {
        this._validation()
    }
    public changeType(type: string) {
        switch (type) {
            case 'old': {
                this.isOld = !this.isOld;
                this.oldIcon = this.isOld ? 'eye' : 'eye-off';
                this.typeOld = this.isOld ? 'text' : 'password'
                break;
            }
            case 'new': {
                this.isNew = !this.isNew;
                this.newIcon = this.isNew ? 'eye' : 'eye-off';
                this.typeNew = this.isNew ? 'text' : 'password'
                break;
            }
            case 'repeat': {
                this.isRepeat = !this.isRepeat;
                this.repeatIcon = this.isRepeat ? 'eye' : 'eye-off';
                this.typeRepeat = this.isRepeat ? 'text' : 'password'
                break;
            }
        }
    }
    private _validation() {
        this.formGroup = this._formBuilder.group({
            oldPassword: ['', Validators.required],
            newPassword: ['', Validators.required],
            repeatPassword: ['', Validators.required]
        })
    }

    //new password
    public enterNewPassword() {
        this.check()
    }
    public dismiss() {
        this._viewCtrl.dismiss()
    }
    public enterRepeatPassword() {
        this.check()
    }
    check() {
        if (this.formGroup.value.repeatPassword && this.formGroup.value.newPassword)
            if (this.formGroup.value.repeatPassword == this.formGroup.value.newPassword) {
                this.isTrue = false
            } else {
                this.isTrue = true
            }
    }
    public changePassword() {
        if (this.formGroup.valid) {
            if (!this.isTrue) {
                this.isError = false
                this._requestService.changePassword(this.formGroup.value.oldPassword, this.formGroup.value.newPassword)
                    .subscribe((data) => {
                        console.log(data);
                        this._viewCtrl.dismiss()
                    },
                        (error) => {
                            this.isError = true
                            this.errorMessage = error.error.error
                        })
            }
        } else {
            this.isError = true
            this.errorMessage = 'Please fill all fields'
        }
    }
}