import { Component, ViewChild, ElementRef, Inject } from '@angular/core';
import { NavController, NavParams, ViewController, ActionSheetController, normalizeURL, Platform, Content, LoadingController, Loading } from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { Crop } from '@ionic-native/crop';
import { File } from '@ionic-native/file';
import { FileTransfer, FileTransferObject, FileUploadOptions } from '@ionic-native/file-transfer';
import { RequestService } from '../../services/request.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NewEvent } from '../../types/types';
import { DatePipe } from '@angular/common';
import moment from 'moment';
declare var cordova;
@Component({
  selector: 'page-create-event',
  templateUrl: 'create-event.html',
})
export class CreateEventPage {
  @ViewChild(Content) content: Content;
  public eventName: string;
  public description: string;
  public startDay: string;
  public startMonth: string;
  public startYear: string;
  public startTime: string;
  public endDay: string;
  public endMonth: string;
  public endYear: string;
  public endTime: string;
  public location: string;
  private _loading: Loading;
  public image: string;
  public defaultImage: string;
  public lastImage: string;
  public noteImage: string;
  public formGroup: FormGroup;
  public id: number;
  public eventInfo: NewEvent;
  public isError: boolean = false
  constructor(
    public navCtrl: NavController,
    private _file: File,
    private _crop: Crop,
    private _platform: Platform,
    private _transfer: FileTransfer,
    private _camera: Camera,
    private _actionsheetCtrl: ActionSheetController,
    public navParams: NavParams,
    private _viewCtrl: ViewController,
    private _requestService: RequestService,
    private _formBuilder: FormBuilder,
    public datePipe: DatePipe,
    private _loadCtrl: LoadingController,
    @Inject('BASE_URL') private _baseUrl,
    @Inject('FILE_URL') public fileUrl
  ) {
    this.id = navParams.get('id')
  }
  public dismiss() {
    this._viewCtrl.dismiss();
  }
  ngOnInit() {
    if (this.id) {
      this.lastImage = 'assets/imgs/colors.jpg'
      this._getEvent()
    }
    this._validation()
  }
  private _validation() {
    this.formGroup = this._formBuilder.group({
      eventName: ['', Validators.required],
      description: ['', Validators.required],
      startDay: ['', Validators.required],
      startMonth: ['', Validators.required],
      startYear: ['', Validators.required],
      startTime: ['', Validators.required],
      endDay: ['', Validators.required],
      endMonth: ['', Validators.required],
      endYear: ['', Validators.required],
      endTime: ['', Validators.required],
      location: ['', Validators.required],
    })
  }
  private _getEvent() {
    let loader = this._loadCtrl.create();
    loader.present();
    this._requestService.getEventById(this.id).subscribe((data: { 'messages': Array<NewEvent>, 'status': string }) => {
      console.log(data);
      loader.dismiss();
      this.eventInfo = data.messages[0];
      let newStartDateIndex = this.eventInfo["start-time"].indexOf(' ');
      let startTimeArr = this.eventInfo["start-time"].substr(0, newStartDateIndex).split('-');
      let newEndDateIndex = this.eventInfo["end-time"].indexOf(' ');
      let endTimeArr = this.eventInfo["end-time"].substr(0, newEndDateIndex).split('-');

      this.eventName = this.eventInfo.name;
      this.description = this.eventInfo.description;
      this.startDay = moment(startTimeArr[2], 'DD').format();
      this.startMonth = moment(startTimeArr[1], 'MM').format();
      this.startYear = startTimeArr[0];
      this.startTime = this.eventInfo["start-time"].substr(newStartDateIndex + 1)
      this.endDay = moment(endTimeArr[2], 'DD').format();
      this.endMonth = moment(endTimeArr[1], 'MM').format();
      this.endYear = endTimeArr[0];
      this.endTime = this.eventInfo["end-time"].substr(newEndDateIndex + 1)
      this.location = this.eventInfo.address;
      if (this.eventInfo.image) {
        this.lastImage = "url(" + this.fileUrl + this.eventInfo.image + ")";
      }
    })
  }


  scrollTo(elementId: string) {
    if (document.getElementById(elementId))
      setTimeout(() => {
        let y = document.getElementById(elementId).offsetTop;
        this.content.scrollTo(0, y);
      }, 500);
  }

  openeditprofile() {
    let actionSheet = this._actionsheetCtrl.create({
      cssClass: 'action-sheets-basic-page',
      buttons: [
        {
          text: 'Take photo',
          role: 'destructive',
          handler: () => {
            this.getImage(1)
            //this.takephoto();
          }
        },
        {
          text: 'Choose photo from Gallery',
          handler: () => {
            this.getImage(0)
            // this.openGallery();
          }
        },
      ]
    });
    actionSheet.present();
  }
  getImage(type) {
    const options: CameraOptions = {
      quality: 100,
      destinationType: this._camera.DestinationType.FILE_URI,
      sourceType: type,
      mediaType: this._camera.MediaType.PICTURE,
      correctOrientation: true,
      allowEdit: true
    }

    this._camera.getPicture(options).then((imageData) => {
      console.log(imageData);
      this.noteImage = imageData;
      this.lastImage = "url(" + imageData + ")"
      // var sourceDirectory = imageData.substring(0, imageData.lastIndexOf('/') + 1);
      // var sourceFileName = imageData.substring(imageData.lastIndexOf('/') + 1, imageData.length);
      // sourceFileName = sourceFileName.split('?').shift();
      // this._file.copyFile(sourceDirectory, sourceFileName,
      //   cordova.file.externalApplicationStorageDirectory, sourceFileName).then((result: any) => {
      //     console.log(result);
      //     this.lastImage = "url(" + result.nativeURL + ")"
      //     console.log(this.lastImage);
      //   })
    }, (err) => {
      console.log(err);
    });
  }


  public uploadFile(id: number) {
    // let postData = new FormData();
    // postData.append('photo', this.lastImage)

    // this._requestService.addEventImage(id, postData).subscribe((data) => {
    //   console.log(data);
    //   this._viewCtrl.dismiss(true)
    // })
    let accessToken = JSON.parse(localStorage.getItem('accessToken')) || '';
    const fileTransfer: FileTransferObject = this._transfer.create()
    let fileName = this.lastImage.split('/').pop();
    console.log(fileName);
    // let paramsdata = {
    //   'photo': this.lastImage
    // }
    let options: FileUploadOptions = {
      fileKey: 'photo',
      fileName: 'fileName',
      chunkedMode: false,
      mimeType: "multipart/form-data",
      httpMethod: 'post',
      headers: {
        'Authorization': 'Bearer ' + accessToken
      }
      // params: []
    }
    fileTransfer.upload(this.noteImage, this._baseUrl + 'event/upload/' + id, options).then((data) => {
      console.log(data);
      
      this._loading.dismiss()
      this._viewCtrl.dismiss(true)
    })
    .catch((error)=>{
      console.log(error);
      
    })
  }

  public createEvent() {
    console.log(this.lastImage);
    console.log(this.formGroup);
    console.log(this.startDay);
    if (this.formGroup.valid && this.lastImage) {
      this._loading = this._loadCtrl.create()
      this._loading.present()
      this.isError = false
      let newStartDay = this.startDay.length > 3 ? this.startDay.substr(8, 2) : this.startDay
      let newStartMonth = this.startMonth.length > 3 ? this.startMonth.substr(5, 2) : this.startMonth;
      let newEndDay = this.endDay.length > 3 ? this.endDay.substr(8, 2) : this.endDay;
      let newEndMonth = this.endMonth.length > 3 ? this.endMonth.substr(5, 2) : this.endMonth;
      let startDate = this.startYear + '-' + newStartMonth + '-' + newStartDay + ' ' + this.startTime;
      let endDate = this.endYear + '-' + newEndMonth + '-' + newEndDay + ' ' + this.endTime;
      console.log(startDate);
      console.log(endDate);
      if (!this.id) {
        this._requestService.addEvent(this.eventName, this.description, startDate, endDate, this.location).subscribe((data: any) => {
          console.log(data.eventChat.id);
          console.log(data);
          this.uploadFile(data.eventChat.id)
        })
      } else {
        console.log(this.lastImage);

        this._requestService.changeEvent(this.id, this.eventName, this.description, startDate, endDate, this.location).subscribe((data) => {
          console.log(data);
          if (this.lastImage.indexOf(this.fileUrl) !== -1) {
            this._loading.dismiss()
            this._viewCtrl.dismiss(true)
          } else {
            this.uploadFile(this.id)
          }
        })
      }
    } else {
      this.isError = true
    }


  }
  endMonthChange(ev) {
    this.endMonth = ev.month;
  }
  startMonthChange(ev) {
    this.startMonth = ev.month
  }
  endDayChange(ev) {
    this.endDay = ev.day;
  }
  startDayChange(ev) {
    this.startDay = ev.day
  }


  // public takephoto() {
  //   var optionsCamera: CameraOptions = {
  //     quality: 100,
  //     targetWidth: 600,
  //     sourceType: this._camera.PictureSourceType.CAMERA,
  //     destinationType: this._camera.DestinationType.DATA_URL,
  //     encodingType: this._camera.EncodingType.JPEG,
  //     mediaType: this._camera.MediaType.PICTURE
  //   }
  //   this._camera.getPicture(optionsCamera).then((imageData) => {
  //     let base64Image = 'data:image/jpeg;base64,' + imageData;
  //     this.lastImage = "url(" + base64Image + ")";
  //   }, (err) => {
  //     // Handle error
  //     console.log(err)
  //   })
  // }
  // openGallery() {
  //   const options: CameraOptions = {
  //     quality: 50,
  //     destinationType: this._camera.DestinationType.DATA_URL,
  //     encodingType: this._camera.EncodingType.JPEG,
  //     mediaType: this._camera.MediaType.PICTURE,
  //     correctOrientation: true,
  //     sourceType: 0,
  //     allowEdit: true
  //   }

  //   this._camera.getPicture(options).then((imageData) => {
  //     let base64Image = 'data:image/jpeg;base64,' + imageData;
  //     this.lastImage = "url(" + base64Image + ")";
  //   }, (err) => {
  //     // Handle error
  //   });

  // }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CreateEventPage');
  }

}
