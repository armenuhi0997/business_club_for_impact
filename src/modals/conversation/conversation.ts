import { Component, ViewChild, ElementRef, Inject } from '@angular/core';
import { NavController, NavParams, ViewController, Content, List, LoadingController, Platform, ToastController } from 'ionic-angular';
import { FileChooser } from '@ionic-native/file-chooser';
import { FileTransfer, FileTransferObject, FileUploadOptions } from '@ionic-native/file-transfer';
import { FilePath } from '@ionic-native/file-path';
import { File } from '@ionic-native/file';
import { RequestService } from '../../services/request.service';
import { Message, EnterUser } from '../../types/types';
import { Subscription } from "rxjs";
import { map } from "rxjs/operators";
import { forkJoin } from 'rxjs/observable/forkJoin';
import { FileOpener } from '@ionic-native/file-opener';
import { DocumentViewer } from '@ionic-native/document-viewer';
declare var cordova;
@Component({
  selector: 'page-conversation',
  templateUrl: 'conversation.html',
})
export class ConversationModal {
  @ViewChild(Content) content: Content;
  @ViewChild(List, { read: ElementRef }) chatList: ElementRef;
  private _mutationObserver: MutationObserver;
  public conversationName: string;
  public clubName: string;
  // public downloadText: string = "";
  // public uploadText: string = "";
  public fileTransfer: FileTransferObject;
  private _conversationId: number;
  public message: string;
  public userId: number;
  public unReadCount: number
  private _subscription: Subscription;
  public conversation: Message[] = [];
  public isGet: boolean = false;
  public isNew: boolean = false;
  public clubId: number;
  public isCheckSendButton: boolean = true;
  public isClickFileIcon:boolean=true;
  constructor(public navCtrl: NavController,
    private _fileChooser: FileChooser,
    public navParams: NavParams,
    private _viewCtrl: ViewController,
    private _file: File,
    private _transfer: FileTransfer,
    private _filePath: FilePath,
    private _requestService: RequestService,
    @Inject('BASE_URL') private _baseUrl,
    @Inject('FILE_URL') public fileUrl,
    private _loadCtrl: LoadingController,
    private _platform: Platform,
    private _fileOpener: FileOpener,
    private _document: DocumentViewer,
    private _toastCtrl: ToastController
  ) {
    this.conversationName = this.navParams.get('name');
    this.clubName = this.navParams.get('clubName');
    this._conversationId = this.navParams.get('conversationId');
    this.clubId = this.navParams.get('clubId');
    this.unReadCount = this.navParams.get('count')
    console.log(this.unReadCount);

  }
  ngOnInit() {
    this._combineObservable()
  }
  ionViewDidLoad() {
    this._scrollToBottom()
  }
  private _combineObservable() {
    const combine = forkJoin(
      this._getMessages(),
      this._getUser(),
      this._readMessage()
    )
    this._subscription = combine.subscribe((data) => {
      this.isGet = true
    })
  }
  dismiss() {
    this._viewCtrl.dismiss(this.isNew)
  }
  private _getMessages(bool?: boolean) {
    let loading = this._loadCtrl.create()
    loading.present()
    return this._requestService.getMessage(this._conversationId).pipe(map((data: { message: Message[] }) => {
      this.conversation = data.message;
      if (bool) {
        setTimeout(() => {
          if (this.content)
            this.content.scrollToBottom();
        }, 300);
      }
      console.log(data);
      loading.dismiss()
      return data.message
    }))
  }
  private _getUser() {
    return this._requestService.getUser().pipe(map((data: { user: EnterUser }) => {
      console.log(data);
      this.userId = data.user.id
      return data
    }))
  }
  private _readMessage() {
    return this._requestService.deleteOneUnreadMessage(this.clubId, this._conversationId).pipe(
      map((data) => {
        console.log(data);
        if (this.unReadCount)
          this.isNew = true
        return data
      }))
  }

  private _scrollToBottom() {
    this._mutationObserver = new MutationObserver((mutations) => {
      console.log(mutations);
      this.content.scrollToBottom();
    });
    this._mutationObserver.observe(this.chatList.nativeElement, {
      childList: true
    });
  }
  public sendTextMessage() {
    if (this.message && this.message.trim() !== '')
      this.isCheckSendButton = false
    this._requestService.addTextMessage(this._conversationId, this.message).subscribe((data) => {
      console.log(data);
      this._scrollToBottom();
      this.isNew = true;
      this.message = '';
      this.isCheckSendButton = true
      this._getMessages(true).subscribe()
    })
  }

  public uploadFile() {
    this.isClickFileIcon=false
    this._fileChooser.open().then((uri) => {
      this.isClickFileIcon=true
      console.log('uri ', uri);
      //path of file in device   
      this._filePath.resolveNativePath(uri).then(
        (nativepath) => {

          let fileName = nativepath.substr(nativepath.lastIndexOf('/') + 1);
          console.log('nativepath ', nativepath);
          this.fileTransfer = this._transfer.create();
          let accessToken = JSON.parse(localStorage.getItem('accessToken')) || '';
          let options: FileUploadOptions = {
            fileKey: 'docs',
            fileName: fileName,
            chunkedMode: false,
            mimeType: "multipart/form-data",
            httpMethod: 'post',
            headers: {
              'Authorization': 'Bearer ' + accessToken
            }
          }
          let loading = this._loadCtrl.create()
          loading.present()
          console.log(options);
          this.fileTransfer.upload(nativepath, this._baseUrl + 'documents/' + this._conversationId, options)
            .then((data) => {
              loading.dismiss()
              this.isNew = true;
              console.log('uploaded ', data);
              this._getMessages(true).subscribe();
            },
              (err) => {
                loading.dismiss()
                console.log(err);
              })
        },
        (err) => {
          console.log(JSON.stringify(err));
        })
    },
      (error) => {
        console.log(JSON.stringify(error));
      })
  }

  public download(item: Message) {
    if (item.type == 1) {
      let loading = this._loadCtrl.create()
      loading.present()
      this.fileTransfer = this._transfer.create();
      // here iam mentioned this line this.file.externalRootDirectory is a native pre-defined file path storage. You can change a file path whatever pre-defined method.  
      this.fileTransfer.download(encodeURI(this.fileUrl + item.content),
        this._file.externalRootDirectory + '/Download/' + item.fileName).then((entry) => {
          console.log('download completed: ' + entry.toURL());
          loading.dismiss()
          let url = entry.toURL();
          console.log(url);
          let path = this._file.externalRootDirectory + '/Download/' + item.fileName
          this._presentToast('File download successfully and placed in the Download folder')
          let format = url.substr(url.lastIndexOf('.'));
          let mimeTypeArray: { [key: string]: string } = {
            '.png': 'image/png',
            '.pdf': 'application/pdf',
            '.jpg': 'image/jpeg',
            '.jpeg': 'image/jpeg',
            '.jpe': 'image/jpeg',
            '.docx': 'application/rtf'
          };
          setTimeout(() => {
            if (this._platform.is('ios')) {
              this._document.viewDocument(path, mimeTypeArray[format], {});
            } else {
              this._fileOpener.open(path, mimeTypeArray[format])
                .then(() => console.log('File is opened'))
                .catch(e => console.log('Error opening file', e));
            }
          }, 6000)

        }, (error) => {
          //here logging our error its easier to find out what type of error occured.  
          console.log('download failed: ' + JSON.stringify(error));
          loading.dismiss()
          this._presentToast('Download failed')
        });
    }
  }
  private _presentToast(msg) {
    let toast = this._toastCtrl.create({
      message: msg,
      duration: 6000,
      position: 'bottom'
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }

  ngOnDestroy() {
    this._subscription.unsubscribe()
  }
}
