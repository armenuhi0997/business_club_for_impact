import { Component, Inject } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';
import { RequestService } from '../../services/request.service';
import { User } from '../../types/types';

/**
 * Generated class for the InvitePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-invite',
  templateUrl: 'invite.html',
})
export class InvitePage {
  public members: Array<User> = [];
  public id: number;
  public selectedArr = []
  constructor(public navCtrl: NavController,
    private _requestService: RequestService,
    public navParams: NavParams,
    private _viewCtrl: ViewController,
    @Inject('FILE_URL') public fileUrl
  ) {
    this.id = navParams.get("id")
  }
  ngOnInit() {
    this._getUsers()
  }
  public dismiss() {
    this._viewCtrl.dismiss()
  }

  public getCheckedMembers(member: User, strValue?: string) {
    if (!member.isDisable) {
      if (strValue) {
        member.isSelect = !member.isSelect;
        console.log(member.isSelect);
      }
      console.log(this.members);
    }
  }
  private _getUsers() {
    this._requestService.getUsers(this.id).subscribe((data: { users: User[] }) => {
      console.log(data);
      this.members = data.users;
      for (let member of this.members) {
        if (member.eventId && member.eventId == this.id) {
          this.selectedArr.push(member.id)
          member["isSelect"] = true;
          member['isDisable'] = true
        } else {
          member["isSelect"] = false
          member['isDisable'] = false
        }
      }
    })
  }
  public confirmed() {
    let userArr: Array<number> = []
    console.log(this.members);
    console.log(this.selectedArr);
    for (let member of this.members) {
      if (member.isSelect) {
        userArr.push(member.id)
      }
    }

    if (this.selectedArr[0]) {
      userArr = userArr.filter((el) => {
        return this.selectedArr.indexOf(el) < 0;
      });
    }
    console.log(userArr);
    if (userArr.length) {
      this._requestService.addInvite(this.id, userArr).subscribe((data) => {
        console.log(data);
        this._viewCtrl.dismiss()
      })
    }
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad InvitePage');
  }
}
