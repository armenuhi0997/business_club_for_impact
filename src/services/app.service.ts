import { Injectable } from "@angular/core";
import { RequestService } from "./request.service";
import { NewEvent } from "../types/types";

@Injectable()
export class AppService{
    public isCheck:boolean=false;
    public eventCount:number;
    public events:NewEvent[]
    constructor(private _requestService:RequestService){}
    public getEvents() {
        this._requestService.getInviteEvent().subscribe((data: { eventUser: NewEvent[] }) => {
            console.log(data);
            this.events = data.eventUser;
            this.eventCount = this.events.length
        })
    }
}