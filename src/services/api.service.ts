import { Injectable, Inject } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { catchError, map } from 'rxjs/operators';
import { of } from 'rxjs/observable/of';
import { Observable } from "rxjs";

@Injectable()
export class ApiService {
    constructor(private _httpClient: HttpClient,
        @Inject('BASE_URL') private _baseUrl) { }
    /**
    * 
    * @param url - request url
    * @param observe - httpOption for get full response,not required
    * @param responseType - httpOption for get full response on type text
    */
    public get(url: string, observe?, responseType?) {
        let accessToken = JSON.parse(localStorage.getItem('accessToken')) || '';
        let headers = new HttpHeaders({
            'Content-type': 'application/json',
            'Authorization': 'Bearer ' + accessToken,
        })
        let params = { headers: headers };
        if (observe == 'response')
            params['observe'] = 'response';
        if (responseType == 'text')
            params['responseType'] = 'text';
        return this._httpClient.get(this._baseUrl + url, params)

            // .pipe(
            //     catchError((error) => {
            //         console.log(error, url);
            //         if (error.status === 401) {
            //             return this.post('refresh', {})
            //                 .pipe(
            //                     map<any, any>((data: any) => {
            //                         console.log(data);
            //                         if (data && data.new_token) {
            //                             localStorage.setItem('accessToken', JSON.stringify(data.new_token));
            //                             return this.get(url, observe, responseType).pipe(
            //                                 map<any, any>((data) => {
            //                                     return data;
            //                                 })
            //                             )
            //                         }
            //                     },
            //                         (error) => {
            //                             console.log(error);
            //                             if (error) {
            //                                 localStorage.setItem('accessToken', JSON.stringify(error.new_token));
            //                                 return this.get(url, observe, responseType)
            //                             }
            //                         })
            //                 )
            //         }
            //         else {
            //             of()
            //         }
            //     })
            // );
    }
    /**
     * 
     * @param url - request url, 
     * @param body - sending object
     * @param observe - httpOption for get full response
     * @param responseType 
     */
    public post(url: string, body: object, observe?, responseType?) {
        let accessToken = JSON.parse(localStorage.getItem('accessToken')) || '';
        let headers = new HttpHeaders({
            'Content-type': 'application/json',
            'Authorization': 'Bearer ' + accessToken,
        })
        let params = { headers: headers };
        if (observe == 'response')
            params['observe'] = 'response';
        if (responseType == 'text')
            params['responseType'] = 'text';
        return this._httpClient.post(this._baseUrl + url, body, params)
        // .pipe(
        //     catchError((error) => {
        //         console.log(error, url);
        //         if (error.status === 401) {
        //             return this.post('refresh', {})
        //                 .pipe(
        //                     map((data: any) => {
        //                         console.log(data);
        //                         if (data && data.new_token) {
        //                             localStorage.setItem('accessToken', JSON.stringify(data.new_token));
        //                             return (this.post(url, body, observe, responseType))
        //                         }
        //                     },
        //                         (error) => {
        //                             console.log(error);
        //                             if (error) {
        //                                 localStorage.setItem('accessToken', JSON.stringify(error.new_token));
        //                                 return (this.post(url, body, observe, responseType))
        //                             }
        //                         })
        //                 )
        //         }
        //         else {
        //             of()
        //         }
        //     })
        // );

    }

    public postFormData(url: string, formData: FormData, observe?, responseType?) {
        let accessToken = JSON.parse(localStorage.getItem('accessToken')) || '';
        let headers = new HttpHeaders({
            'Authorization': 'Bearer ' + accessToken,
        })
        let params = { headers: headers };
        if (observe == 'response')
            params['observe'] = 'response';
        if (responseType == 'text')
            params['responseType'] = 'text';
        return this._httpClient.post(this._baseUrl + url, formData, params)
        // .pipe(
        //     catchError((error) => {
        //         console.log(error, url);
        //         if (error.status === 401) {
        //             return this.post('refresh', {})
        //                 .pipe(
        //                     map((data: any) => {
        //                         console.log(data);
        //                         if (data && data.new_token) {
        //                             localStorage.setItem('accessToken', JSON.stringify(data.new_token));
        //                             return (this.postFormData(url, formData, observe, responseType))
        //                         }
        //                     },
        //                         (error) => {
        //                             console.log(error);
        //                             if (error) {
        //                                 localStorage.setItem('accessToken', JSON.stringify(error.new_token));
        //                                 return (this.postFormData(url, formData, observe, responseType))
        //                             }
        //                         })
        //                 )
        //         }
        //         else {
        //             of()
        //         }
        //     })
        // );
    }

    /**
     * 
     * @param url 
     * @param body 
     * @param observe 
     * @param responseType 
     */
    public put(url: string, body: object, observe?, responseType?) {
        let accessToken = JSON.parse(localStorage.getItem('accessToken')) || '';
        let headers = new HttpHeaders({
            'Content-type': 'application/json',
            'Authorization': 'Bearer ' + accessToken,
        })
        let params = { headers: headers };
        if (observe == 'response')
            params['observe'] = 'response';
        if (responseType == 'text')
            params['responseType'] = 'text';

        return this._httpClient.put(this._baseUrl + url, body, params)
        // .pipe(
        //     catchError((error) => {
        //         console.log(error, url);
        //         if (error.status === 401) {
        //             return this.post('refresh', {})
        //                 .pipe(
        //                     map((data: any) => {
        //                         console.log(data);
        //                         if (data && data.new_token) {
        //                             localStorage.setItem('accessToken', JSON.stringify(data.new_token));
        //                             return (this.put(url, body, observe, responseType))
        //                         }
        //                     },
        //                         (error) => {
        //                             console.log(error);
        //                             if (error) {
        //                                 localStorage.setItem('accessToken', JSON.stringify(error.new_token));
        //                                 return (this.put(url, body, observe, responseType))
        //                             }
        //                         })
        //                 )
        //         }
        //         else {
        //             of()
        //         }
        //     })
        // );
    }
    /**
     * 
     * @param url 
     * @param observe 
     * @param responseType 
     */
    public delete(url: string, observe?, responseType?) {
        let accessToken = JSON.parse(localStorage.getItem('accessToken')) || '';
        let headers = new HttpHeaders({
            'Content-type': 'application/json',
            'Authorization': 'Bearer ' + accessToken,
        })
        let params = { headers: headers };
        if (observe == 'response')
            params['observe'] = 'response';
        if (responseType == 'text')
            params['responseType'] = 'text';

        return this._httpClient.delete(this._baseUrl + url, params)
        // .pipe(
        //     catchError((error) => {
        //         console.log(error, url);
        //         if (error.status === 401) {
        //             return this.post('refresh', {})
        //                 .pipe(
        //                     map((data: any) => {
        //                         console.log(data);
        //                         if (data && data.new_token) {
        //                             localStorage.setItem('accessToken', JSON.stringify(data.new_token));
        //                             return (this.delete(url, observe, responseType))
        //                         }
        //                     },
        //                         (error) => {
        //                             console.log(error);
        //                             if (error) {
        //                                 localStorage.setItem('accessToken', JSON.stringify(error.new_token));
        //                                 return (this.delete(url, observe, responseType))
        //                             }
        //                         })
        //                 )
        //         }
        //         else {
        //             of()
        //         }
        //     })
        // );
    }

}