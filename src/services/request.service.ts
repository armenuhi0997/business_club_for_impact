import { Injectable, Inject } from "@angular/core";
import { ApiService } from "./api.service";
import { Member, NewEvent, Clubs } from "../types/types";
import { HttpClient } from "@angular/common/http";

@Injectable()
export class RequestService {

    constructor(private _apiService: ApiService, private _httpClient: HttpClient,
        @Inject('BASE_URL') private _baseUrl) { }
    /**
     * 
     * @param name 
     * @param email 
     * @param company 
     * @param phone 
     * @param message 
     */
    public registration(name: string, email: string, company: string, phone: number, message: string) {
        return this._httpClient.post(this._baseUrl + 'register',
            { name: name, email: email, company: company, phone: phone, message: message })
    }
    /**
     * 
     * @param email 
     * @param password 
     */
    public login(email: string, password: string) {
        return this._httpClient.post(this._baseUrl + 'login', { email: email, password: password })
    }
    public checkAccessToken() {
        return this._apiService.get('check', 'response', 'text')
    }
    public getNewToken() {
        return this._apiService.post('refresh', {})
    }
    /**
     * 
     * @param isSearch 
     * @param name 
     */
    public getClubs(isSearch: boolean, name: string) {
        return this._apiService.post('clubs', { "isSearch": isSearch, name: name })
    }
    /**
     * 
     * @param id 
     */
    public getClubsById(id: number) {
        return this._apiService.get(`clubs/${id}`)
    }
    /**
     * 
     * @param clubId 
     */
    public getAllMembersByClubId(isSearch: boolean, name: string, clubId: number) {
        return this._apiService.post('club/members', { "isSearch": isSearch, name: name, clubId: clubId })
    }
    /**
     * 
     * @param clubId 
     * @param memberId 
     */
    public getMemberById(memberId: number) {
        return this._apiService.get(`member/${memberId}`)
    }
    /**
     * 
     * @param isSearch 
     * @param name 
     * @param clubId 
     */
    public getConversationsByClubId(isSearch: boolean, name: string, clubId: number) {
        return this._apiService.post('club/conversations', { "isSearch": isSearch, name: name, clubId: clubId })
    }
    public addConversation(name: string, clubId: number, usersArray: Array<number>) {
        return this._apiService.post('club/conversation', {
            "name": name,
            "clubId": clubId,
            "users": usersArray
        })
    }
    public getEvents(isSearch: boolean, name: string) {
        return this._apiService.post('event', { "isSearch": isSearch, name: name })
    }
    public getEventById(id) {
        return this._apiService.get('event/' + id)
    }
    /**
     * 
     * @param name 
     * @param description 
     * @param startTime 
     * @param endTime 
     * @param address 
     */
    public addEvent(name: string, description: string, startTime: string, endTime: string, address: string) {
        return this._apiService.post('event/add',
            {
                'name': name,
                'description': description,
                'start-time': startTime,
                'end-time': endTime,
                'address': address
            })
    }
    /**
     * 
     * @param id 
     * @param name 
     * @param description 
     * @param startTime 
     * @param endTime 
     * @param address 
     */
    public changeEvent(id: number, name: string, description: string, startTime: string, endTime: string, address: string) {
        return this._apiService.put('event/' + id, {
            'name': name,
            'description': description,
            'start-time': startTime,
            'end-time': endTime,
            'address': address
        })
    }
    /**
     * 
     * @param id 
     */
    public deleteEvent(id: number) {
        return this._apiService.delete('event/' + id)
    }
    public addUserImage(body) {
        return this._apiService.postFormData('user/upload', body, 'response', 'text')
    }
    public addEventImage(id, body) {
        return this._apiService.postFormData('event/add' + id, body, 'response', 'text')
    }
    public getUser() {
        return this._apiService.get('user')
    }
    public changeUser(name, company, phone, email) {
        return this._apiService.put('user', { name: name, company: company, phone: phone, email: email })
    }
    public addInvite(eventId, usersArr) {
        return this._apiService.post('event/invite', { 'eventId': eventId, 'users': usersArr })
    }
    public getUsers(id) {
        return this._apiService.get('users/' + id)
    }
    public getMessage(conversationId: number) {
        return this._apiService.get(`message/${conversationId}`)
    }
    public addTextMessage(conversationId: number, message: string) {
        return this._apiService.post(`message/${conversationId}`, { content: message })
    }
    public getFile(id: number) {
        return this._apiService.get('download/documents/' + id)
    }
    /**
     * 
     * @param clubId 
     */
    public deleteAllUnreadMessageCount(clubId: number) {
        return this._apiService.delete(`club/conversations/${clubId}`)
    }
    /**
     * 
     * @param clubId 
     * @param conversationId 
     */
    public deleteOneUnreadMessage(clubId: number, conversationId: number) {
        return this._apiService.delete(`club/conversations/${clubId}/${conversationId}`)
    }
    public getInviteEvent() {
        return this._apiService.get('user/event')
    }
    /**
     * 
     * @param url 
     * @param eventId 
     */
    public addActionEvent(url: string, eventId: number) {
        return this._apiService.post(`${url}/${eventId}`, {})
    }
    /**
     * 
     * @param oldPassword 
     * @param password 
     */
    public changePassword(oldPassword: string, password: string) {
        return this._apiService.put('password', { oldPassword: oldPassword, password: password })
    }
    public joinClub(clubId: number) {
        return this._apiService.post('club/join', { clubId: clubId })
    }
}